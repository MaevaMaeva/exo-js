# Exo-JS

Exo 1
Comment tu t'appelles ?
>Vérifier le nom (entre 1 et 10 caractères + non vide)
>Répondre "Bonjour XXX !"

Exo 2
Login/MDP
>identifiant de + de 4 caratères et qui contient un @
>identifiant suivant : lea@gmail.com + MDP : a6E353_5

Exo 4
Calcul
>Le programme tire une opération (+,-,x) + 2chiffres de manière aléatoire
>Le programme demande le résultat à l'utilisateur
>Le programme dit si juste ou faux

Exo 5
Moyenne
>Saisir 10 notes
>Afficher la Moyenne

Exo 6
Nombre mystère
>Le programme tire un nombre entre 0 et 10
>L'utilisateur donne un nombre
>Le programme répond +,- ou "gagné !"